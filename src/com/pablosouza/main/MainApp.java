package com.pablosouza.main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

// DD-MM-YYYY HH:MM:SS
// 2020-06-16 22:00:00


public class MainApp extends Application  {

    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/com/pablosouza/controller/sleep.fxml"));
        primaryStage.setTitle("Pablo's Sleep App");
        primaryStage.setScene(new Scene(root, 1200, 800));
        primaryStage.show();
    }
}
