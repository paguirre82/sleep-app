package com.pablosouza.model;

import com.pablosouza.util.DBUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class SleepDAO {

    public static void insertSleep(String sleepStart, String sleepEnd) throws SQLException, ClassNotFoundException {
        int duration = getDuration(sleepStart, sleepEnd);
        String weekDay = getDay(sleepEnd);
        String month = getMonth(sleepEnd);
        int year = getYear(sleepEnd);
        int week = getWeek(sleepEnd);
        String sql = "INSERT INTO newentries(dateStart, dateEnd, duration, day, month, year, week) values ('" + sleepStart + "', '" + sleepEnd + "', '" + duration + "', '" + weekDay + "', '" + month + "', '" + year + "', '" + week + "')";
        try {
            DBUtil.dbExecuteQuery(sql);
        } catch (SQLException e) {
            System.out.println("Exception occur while inserting the data" + e);
            e.printStackTrace();
            throw e;
        }
    }

    public static void deleteEntry(int id) throws SQLException, ClassNotFoundException {
        String sql = "DELETE FROM newentries WHERE ID = '" + id + "' ";

        try {
            DBUtil.dbExecuteQuery(sql);
        } catch (SQLException e) {
            System.out.println("Error while updating");
            e.printStackTrace();
            throw e;
        }
    }

    public static int getDuration(String sleepStart, String sleepEnd) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateStart = null;
        Date dateEnd = null;
        try {
            dateStart = formatter.parse(sleepStart);
            dateEnd = formatter.parse(sleepEnd);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long diffMilli = Math.abs(dateEnd.getTime() - dateStart.getTime());
        long diff = TimeUnit.MINUTES.convert(diffMilli, TimeUnit.MILLISECONDS);
        return (int) diff;
    }

    private static String getDay(String sleepEnd) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateEnd = null;
        try {
            dateEnd = formatter.parse(sleepEnd);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        LocalDate endDate = dateEnd.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        String weekDay = endDate.getDayOfWeek().toString();
        return weekDay;
    }

    public static int getWeek(String endDate) {
        SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        int week = 0;
        try {
            Date d1 = sdformat.parse("2020-06-15 00:00:00");
            Date d2 = sdformat.parse("2020-06-22 00:00:00");
            Date d3 = sdformat.parse("2020-06-29 00:00:00");
            Date t = sdformat.parse(endDate);
            if (t.compareTo(d1) >= 0 && t.compareTo(d2) < 0) {
//                System.out.println("this is week 25");
                week = 25;
            } else if (t.compareTo(d2) >= 0 && t.compareTo(d3) < 0){
                week = 26;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return week;
    }

    public static String getMonth(String sleepEnd) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateEnd = null;
        try {
            dateEnd = formatter.parse(sleepEnd);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        LocalDate endDate = dateEnd.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        String month = endDate.getMonth().toString();
        return month;
    }

    public static Integer getYear(String sleepEnd) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateEnd = null;
        try {
            dateEnd = formatter.parse(sleepEnd);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        LocalDate endDate = dateEnd.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int year = endDate.getYear();
        return year;
    }

    /*
     * Gets resultset from dbUtil, passing the query
     * Creates obs list applying getSleepObjects to resultset
     */

    public static ObservableList<Sleep> getAllRecords() throws ClassNotFoundException, SQLException {
        String sql = "SELECT * from newentries";

        try {
            ResultSet rsSet = DBUtil.dbRetrieve(sql);
            ObservableList<Sleep> entryList = getSleepObjects(rsSet);
            return entryList;
        } catch (SQLException e) {
            System.out.println("Error fetching DB");
            e.printStackTrace();
            throw e;
        }
    }

    /*
     creates an ObservableList, loops through resultset and populates it with sleep objects
     */
    public static ObservableList<Sleep> getSleepObjects(ResultSet rsSet) throws ClassNotFoundException, SQLException {

        try {
            ObservableList<Sleep> sleepList = FXCollections.observableArrayList();

            while (rsSet.next()) {
                Sleep sleep = new Sleep();
                sleep.setIdProperty(rsSet.getInt("id"));
                sleep.setStartProperty(rsSet.getString("dateStart"));
                sleep.setEndProperty(rsSet.getString("dateEnd"));
                sleep.setDuration(rsSet.getInt("duration"));
                sleep.setDayProperty(rsSet.getString("day"));
                sleep.setMonthProperty(rsSet.getString("month"));
                sleep.setYearProperty(rsSet.getString("year"));
                sleep.setWeekProperty(rsSet.getString("week"));

                sleepList.add(sleep);
            }
            return sleepList;

        } catch (SQLException e) {
            System.out.println("Error fetching DB" + e);
            e.printStackTrace();
            throw e;
        }
    }

    public static ObservableList<Sleep> searchByWeek(String week) throws ClassNotFoundException, SQLException {
        String sql = "SELECT * FROM newentries WHERE week LIKE '" + week + "' ";
        try {
            ResultSet rs = DBUtil.dbRetrieve(sql);
            ObservableList<Sleep> entryList = getSleepObjects(rs);
            return entryList;
        } catch (SQLException e) {
            System.out.println("Error fetching DB" + e);
            e.printStackTrace();
            throw e;
        }
    }

    public static ObservableList<Sleep> searchByMonth(String month) throws ClassNotFoundException, SQLException {
        String sql = "SELECT * FROM newentries WHERE month LIKE '" + month + "' ";
        try {
            System.out.println(sql);
            ResultSet rs = DBUtil.dbRetrieve(sql);
            ObservableList<Sleep> entryList = getSleepObjects(rs);
            return entryList;
        } catch (SQLException e) {
            System.out.println("Error fetching DB" + e);
            e.printStackTrace();
            throw e;
        }
    }

    public static ObservableList<Sleep> searchByDay(String day) throws ClassNotFoundException, SQLException {
        String sql = "SELECT * FROM newentries WHERE day LIKE '" + day + "' ";
        try {
            ResultSet rs = DBUtil.dbRetrieve(sql);
            ObservableList<Sleep> entryList = getSleepObjects(rs);
            return entryList;
        } catch (SQLException e) {
            System.out.println("Error fetching DB" + e);
            e.printStackTrace();
            throw e;
        }
    }

//    public static int getSumOfDurations() throws ClassNotFoundException, SQLException {
//        String sql = "SELECT * from entries";
//
//        try {
//            ResultSet rsSet = DBUtil.dbRetrieve(sql);
//            ObservableList<Sleep> entryList = getSleepObjects(rsSet);
//            entryList.forEach(System.out::println);
//            int sum = 0;
//
//            for (int i=0; i < entryList.size(); i++) {
//                sum = sum + entryList.get(i).getDuration();
//            }
//
//            System.out.println("Sum of durations: " + sum);
//            return sum;
//
//        } catch (SQLException e) {
//            System.out.println("Error fetching DB");
//            e.printStackTrace();
//            throw e;
//        }
//    }
}
