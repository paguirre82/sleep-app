package com.pablosouza.model;

import com.pablosouza.util.DBUtil;
import javafx.collections.ObservableList;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class WeekDAO {
    public static ObservableList<Sleep> getWeekRecords() throws ClassNotFoundException, SQLException {
        String sql = "SELECT * from week25";

        try {
            ResultSet rsSet = DBUtil.dbRetrieve(sql);
            ObservableList<Sleep> entryList = SleepDAO.getSleepObjects(rsSet);
            return entryList;
        } catch (SQLException e) {
            System.out.println("Error fetching DB");
            e.printStackTrace();
            throw e;
        }
    }

    public static boolean getWeek(String endDate) {
        SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        boolean week = false;
        try {
            Date d1 = sdformat.parse("2020-06-15 00:00:00");
            Date d2 = sdformat.parse("2020-06-22 00:00:00");
            Date t = sdformat.parse(endDate);
            if (t.compareTo(d1) >= 0 && t.compareTo(d2) < 0) {
                System.out.println("found one!");
                week = true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return week;
    }

    public static ObservableList<Sleep> searchEntries(String week) throws ClassNotFoundException, SQLException {
        String sql = "SELECT * FROM week" + week;
        try {
            ResultSet rs = DBUtil.dbRetrieve(sql);
            ObservableList<Sleep> entryList = SleepDAO.getSleepObjects(rs);
            return entryList;
        } catch (SQLException e) {
            System.out.println("Error fetching DB" + e);
            e.printStackTrace();
            throw e;
        }
    }

    public static void deleteEntry(String weekId, int id) throws SQLException, ClassNotFoundException {
        String sql = "DELETE FROM week" + weekId + " WHERE ID = '" + id + "' ";

        try {
            DBUtil.dbExecuteQuery(sql);
        } catch (SQLException e) {
            System.out.println("Error while updating");
            System.out.println(sql);
            e.printStackTrace();
            throw e;
        }
    }
}
