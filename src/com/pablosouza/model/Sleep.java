package com.pablosouza.model;

import javafx.beans.property.*;

public class Sleep {

    private IntegerProperty idProperty;

    private StringProperty startProperty; // "Property" mandatory for tableview

    private StringProperty endProperty;

    private IntegerProperty duration;

    private StringProperty dayProperty;

    private StringProperty monthProperty;

    private StringProperty yearProperty;

    private StringProperty weekProperty;

    public Sleep() {
        this.idProperty = new SimpleIntegerProperty();
        this.startProperty = new SimpleStringProperty();
        this.endProperty = new SimpleStringProperty();
        this.duration = new SimpleIntegerProperty();
        this.dayProperty = new SimpleStringProperty();
        this.monthProperty = new SimpleStringProperty();
        this.yearProperty = new SimpleStringProperty();
        this.weekProperty = new SimpleStringProperty();
    }

    public String getDayProperty() {
        return dayProperty.get();
    }

    public StringProperty dayProperty() {
        return dayProperty;
    }

    public void setDayProperty(String dayProperty) {
        this.dayProperty.set(dayProperty);
    }

    public String getMonthProperty() {
        return monthProperty.get();
    }

    public StringProperty monthProperty() {
        return monthProperty;
    }

    public void setMonthProperty(String monthProperty) {
        this.monthProperty.set(monthProperty);
    }

    public String getYearProperty() {
        return yearProperty.get();
    }

    public StringProperty yearProperty() {
        return yearProperty;
    }

    public void setYearProperty(String yearProperty) {
        this.yearProperty.set(yearProperty);
    }

    public String getWeekProperty() {
        return weekProperty.get();
    }

    public StringProperty weekProperty() {
        return weekProperty;
    }

    public void setWeekProperty(String weekProperty) {
        this.weekProperty.set(weekProperty);
    }

    public int getIdProperty() {
        return idProperty.get();
    }

    public void setIdProperty(int idProperty) {
        this.idProperty.set(idProperty);
    }

    public IntegerProperty idProperty() {
        return idProperty;
    }

    public String getStartProperty() {
        return startProperty.get();
    }

    public void setStartProperty(String startProperty) {
        this.startProperty.set(startProperty);
    }

    public StringProperty startProperty() {
        return startProperty;
    }

    public String getEndProperty() {
        return endProperty.get();
    }

    public void setEndProperty(String endProperty) {
        this.endProperty.set(endProperty);
    }

    public StringProperty endProperty() {
        return endProperty;
    }

    public int getDuration() {
        return duration.get();
    }

    public void setDuration(int duration) {
        this.duration.set(duration);
    }

    public IntegerProperty durationProperty() {
        return duration;
    }

    @Override
    public String toString()
    {
        return endProperty.toString();
    }


}
