package com.pablosouza.model;

import com.pablosouza.util.DBUtil;
import javafx.collections.ObservableList;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class UpdateWeeks {
    public static String test() {
       return "I'm UpdateWeeks class";
    }

    public static String getAllSleeps() throws ClassNotFoundException, SQLException {
        String myReturn="";

        ObservableList<Sleep> entryList = SleepDAO.getAllRecords();

       for (int i=0; i < entryList.size(); i++) {
//           System.out.println(entryList.get(i).endProperty().getValue().toString());
           if (getWeek(entryList.get(i).endProperty().getValue())) {
               updateWeek(entryList.get(i).endProperty().getValue());
           }
       }
        return "success";
    }

    public static boolean getWeek(String endDate) {
        SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        boolean week = false;
        try {
            Date d1 = sdformat.parse("2020-06-15 00:00:00");
            Date d2 = sdformat.parse("2020-06-22 00:00:00");
            Date t = sdformat.parse(endDate);
            if (t.compareTo(d1) >= 0 && t.compareTo(d2) < 0) {
//                System.out.println("this is week 25");
                week = true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return week;
    }

    private static void updateWeek(String sleepStart) throws SQLException, ClassNotFoundException {
        String sql = "UPDATE week26 SET sleepStart = ' " + sleepStart + " '";

        try {
            DBUtil.dbExecuteQuery(sql);
        } catch (SQLException e) {
            System.out.println("Error while updating");
            e.printStackTrace();
            throw e;
        }
    }
}
