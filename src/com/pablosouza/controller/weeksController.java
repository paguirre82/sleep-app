package com.pablosouza.controller;

import com.pablosouza.model.Sleep;
import com.pablosouza.model.SleepDAO;
import com.pablosouza.model.WeekDAO;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;

public class weeksController {
    @FXML
    private TableColumn<Sleep, Integer> colSleepIdWeek;

    @FXML
    private TableColumn<Sleep, String> colStartWeek;

    @FXML
    private TableColumn<Sleep, String> colEndWeek;

    @FXML
    private TableColumn<Sleep, Integer> colDurationWeek;

    @FXML
    private TableView entryTableWeek;

    @FXML
    private TextField week;

    @FXML
    private TextField weekId;

    @FXML
    private TextField deleteIdWeekTable;

    @FXML
    private void initialize() throws Exception {
    }

    @FXML
    public void changeToMainScene (ActionEvent event) {
        try {
            Parent sleepView = FXMLLoader.load(getClass().getResource("/com/pablosouza/controller/sleep.fxml"));
            Scene tableViewScene = new Scene(sleepView);
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
            window.setScene(tableViewScene);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void populateWeek(ObservableList<Sleep> weekList) {
        colSleepIdWeek.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
        colStartWeek.setCellValueFactory(cellData -> cellData.getValue().startProperty());
        colEndWeek.setCellValueFactory(cellData -> cellData.getValue().endProperty());
        colDurationWeek.setCellValueFactory(cellData -> cellData.getValue().durationProperty().asObject());
        entryTableWeek.setItems(weekList);
    }

    @FXML
    private void searchWeek(ActionEvent event) throws ClassNotFoundException, SQLException {
        ObservableList<Sleep> list = WeekDAO.searchEntries(week.getText());
        populateWeek(list);
    }

    @FXML
    private void deleteEntryWeek(ActionEvent event) throws ClassNotFoundException, SQLException {
        try {
            WeekDAO.deleteEntry(weekId.getText(), Integer.parseInt(deleteIdWeekTable.getText()));
            ObservableList<Sleep> entryList = WeekDAO.getWeekRecords();
            populateWeek(entryList);
        } catch (SQLException e) {
            System.out.println("Error while deleting");
            e.printStackTrace();
            throw e;
        }
    }
}
