package com.pablosouza.controller;

import com.pablosouza.model.Sleep;
import com.pablosouza.model.SleepDAO;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;


public class SleepController {

    private String completeDateStart;

    private String completeDateEnd;

    @FXML
    private TextArea console;

    @FXML
    private TextField deleteId;

    @FXML
    private ChoiceBox searchWeeks;

    @FXML
    private ChoiceBox searchMonths;

    @FXML
    private ChoiceBox searchDay;

    @FXML
    private TableColumn<Sleep, Integer> colSleepId;

    @FXML
    private TableColumn<Sleep, String> colStart;

    @FXML
    private TableColumn<Sleep, String> colEnd;

    @FXML
    private TableColumn<Sleep, Integer> colDuration;

    @FXML
    private TableColumn<Sleep, String> colDay;

    @FXML
    private TableColumn<Sleep, String> colWeek;

    @FXML
    private TableColumn<Sleep, String> colMonth;

    @FXML
    private TableColumn<Sleep, String> colYear;

    @FXML
    private TableView entryTable;

    @FXML
    private DatePicker dateStart;

    @FXML
    private DatePicker dateEnd;

    @FXML
    private ChoiceBox hoursStart;

    @FXML
    private ChoiceBox minutesStart;

    @FXML
    private ChoiceBox hoursEnd;

    @FXML
    private ChoiceBox minutesEnd;

    @FXML
    private void insertSleep(ActionEvent event) throws ClassNotFoundException, SQLException {
        completeDateStart = dateStart.getValue().toString() +" "+hoursStart.getValue().toString()+":"+minutesStart.getValue().toString()+":00";
        completeDateEnd = dateEnd.getValue().toString() +" "+hoursEnd.getValue().toString()+":"+minutesEnd.getValue().toString()+":00";
        try {
            SleepDAO.insertSleep(completeDateStart, completeDateEnd);
            console.setText(SleepDAO.getMonth(completeDateEnd));
            ObservableList<Sleep> entryList = SleepDAO.getAllRecords();
            populateTable(entryList);
        } catch (SQLException e) {
            System.out.println("Exception occurred in insertion " + e);
            e.printStackTrace();
            throw e;
        }
    }

    @FXML
    private void deleteEntry(ActionEvent event) throws ClassNotFoundException, SQLException {
        try {
            SleepDAO.deleteEntry(Integer.parseInt(deleteId.getText()));
            console.setText("Success! Deleted!");
            ObservableList<Sleep> entryList = SleepDAO.getAllRecords();
            populateTable(entryList);
        } catch (SQLException e) {
            System.out.println("Error while deleting");
            e.printStackTrace();
            throw e;
        }
    }

    @FXML
    private void initialize() throws Exception {
        colSleepId.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
        colStart.setCellValueFactory(cellData -> cellData.getValue().startProperty());
        colEnd.setCellValueFactory(cellData -> cellData.getValue().endProperty());
        colDuration.setCellValueFactory(cellData -> cellData.getValue().durationProperty().asObject());
        colDay.setCellValueFactory(cellData -> cellData.getValue().dayProperty());
        colWeek.setCellValueFactory(cellData -> cellData.getValue().weekProperty());
        colMonth.setCellValueFactory(cellData -> cellData.getValue().monthProperty());
        colYear.setCellValueFactory(cellData -> cellData.getValue().yearProperty());
        ObservableList<Sleep> entryList = SleepDAO.getAllRecords();
        populateTable(entryList);

        hoursStart.getItems().addAll("00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17","18", "19", "20", "21", "22", "23");


        minutesStart.getItems().addAll("00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17","18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41","42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59");

        hoursEnd.getItems().addAll("00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17","18", "19", "20", "21", "22", "23");


        minutesEnd.getItems().addAll("00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17","18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41","42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59");

        searchWeeks.getItems().addAll( "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17","18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41","42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52");

        searchMonths.getItems().addAll("JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER");

        searchDay.getItems().addAll("MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY", "SUNDAY");

    }

    private void populateTable(ObservableList<Sleep> entryList) {
        entryTable.setItems(entryList);
    }


    @FXML
    private void searchByWeek(ActionEvent event) throws ClassNotFoundException, SQLException {
        ObservableList<Sleep> list = SleepDAO.searchByWeek(searchWeeks.getValue().toString());
        populateTable(list);
    }

    @FXML
    private void searchByMonth(ActionEvent event) throws ClassNotFoundException, SQLException {
        ObservableList<Sleep> list = SleepDAO.searchByMonth(searchMonths.getValue().toString());
        populateTable(list);
    }

    @FXML
    private void searchByWeekDay(ActionEvent event) throws ClassNotFoundException, SQLException {
        ObservableList<Sleep> list = SleepDAO.searchByDay(searchDay.getValue().toString());
        populateTable(list);
    }

    @FXML
    private void searchAllEntries (ActionEvent event) throws ClassNotFoundException, SQLException {
        ObservableList<Sleep> list = SleepDAO.getAllRecords();
        populateTable(list);
        console.setText("Showing all records!");
    }


    @FXML
    public void changeToWeekScene (ActionEvent event) {
        try {
            Parent weeksView = FXMLLoader.load(getClass().getResource("/com/pablosouza/controller/weeks.fxml"));
            Scene tableViewScene = new Scene(weeksView);
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
            window.setScene(tableViewScene);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}