package com.pablosouza.util;

import com.sun.rowset.CachedRowSetImpl;

import java.sql.*;
import java.util.TimeZone;

public class DBUtil {
    private static String JDBC_DRIVER = "com.mysql.jdbc.Driver"; //step 1

    private static Connection connection = null;

    private static String connStr = "jdbc:mysql://localhost:3306/sleep?serverTimezone=" + TimeZone.getDefault().getID();

    /**
     * called by
     * dbExecutequery (local)
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static void dbConnect() throws SQLException, ClassNotFoundException {
        try {
            Class.forName(JDBC_DRIVER);
        } catch (ClassNotFoundException e) {
            System.out.println("Where is your driver?");
            e.printStackTrace();
            throw e;
        }

        System.out.println("Driver registered!");

        try {
            connection = DriverManager.getConnection(connStr, "root", "123456");
        } catch (SQLException e) {
            System.out.println("Connection Failed!");
            e.printStackTrace();
        }
    }

    public static void dbDisconnect() throws SQLException {
        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * Insert, Update and Delete
     * @param sql
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static void dbExecuteQuery(String sql) throws SQLException, ClassNotFoundException {
        Statement statement = null;
        try {
            dbConnect();
            statement = connection.createStatement();
            statement.executeUpdate(sql);

        } catch (SQLException e) {
            System.out.println("Problem occurred in dbExecuteQuery" + e);
            throw e;
        } finally {
            if (statement != null) {
                statement.close();
            }
            dbDisconnect();
        }
    }

    public static ResultSet dbRetrieve(String query) throws ClassNotFoundException, SQLException{
        Statement statement = null;
        ResultSet resultSet = null;
        CachedRowSetImpl crs = null;

        try {
            dbConnect();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            crs = new CachedRowSetImpl();
            crs.populate(resultSet);

        } catch (SQLException e) {
            System.out.println("Error occured in dbRetrieve operation"+ e);
            throw e;
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (statement != null) {
                statement.close();
            }
            dbDisconnect();
        }
        return crs;
    }

}